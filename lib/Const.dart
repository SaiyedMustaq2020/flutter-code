import 'package:flutter/cupertino.dart';

double appbarHeight(BuildContext context) {
  return MediaQuery.of(context).padding.top;
}

double safeAreaHeight(BuildContext context) {
  return MediaQuery.of(context).padding.bottom;
}

//const textColor = Color(383838);

const Color textColor = Color(0xff383838);
