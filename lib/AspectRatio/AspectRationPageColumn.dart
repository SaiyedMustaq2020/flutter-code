import 'package:flutter/material.dart';

class AspectRationPageColumn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: AspectRationPageScreen(),
    );
  }
}

class AspectRationPageScreen extends StatefulWidget {
  @override
  _AspectRationPageScreenState createState() => _AspectRationPageScreenState();
}

class _AspectRationPageScreenState extends State<AspectRationPageScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            AspectRatio(
              aspectRatio: 1.2,
              child: Expanded(
                child: Container(
                  color: Colors.red,
                ),
              ),
            ),
            AspectRatio(
              aspectRatio: 1.2,
              child: Expanded(
                child: Container(
                  color: Colors.yellow,
                ),
              ),
            ),
            AspectRatio(
              aspectRatio: 1.2,
              child: Expanded(
                child: Container(
                  color: Colors.grey,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
