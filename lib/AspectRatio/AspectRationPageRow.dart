import 'package:flutter/material.dart';

class AspectRationPageRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: AspectRationPageScreen(),
    );
  }
}

class AspectRationPageScreen extends StatefulWidget {
  @override
  _AspectRationPageScreenState createState() => _AspectRationPageScreenState();
}

class _AspectRationPageScreenState extends State<AspectRationPageScreen> {
  @override
  Widget build(BuildContext context) {
    double statusBarHeight = MediaQuery.of(context).padding.top;
    double safeBarHeight = MediaQuery.of(context).padding.bottom;

    return Scaffold(
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: statusBarHeight, bottom: safeBarHeight),
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            AspectRatio(
              aspectRatio: 0.2,
              child: Expanded(
                child: Container(
                  color: Colors.red,
                ),
              ),
            ),
            AspectRatio(
              aspectRatio: 0.2,
              child: Expanded(
                child: Container(
                  color: Colors.yellow,
                ),
              ),
            ),
            AspectRatio(
              aspectRatio: 0.2,
              child: Expanded(
                child: Container(
                  color: Colors.grey,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
