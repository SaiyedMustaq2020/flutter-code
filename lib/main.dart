import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:provider_demo/textScale/TextScalePerDevice.dart';

import 'AbsorbPointer/AbsorbPointerPage.dart';
import 'AspectRatio/AspectRationPageColumn.dart';
import 'AspectRatio/AspectRationPageRow.dart';
import 'PostDataProvider.dart';
import 'ShimmerEffect.dart';

void main() {
  List<SingleChildWidget> providers = [
    ChangeNotifierProvider<PostDataProvider>(
      create: (_) => PostDataProvider(),
    ),
  ];
  runApp(
    MultiProvider(
      providers: providers,
      child: MyProviderDemoScreen(),
    ),
  );
}

class MyProviderDemoScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: TextScaleFactor(),
    );
  }
}

class ProviderDemoScreen extends StatefulWidget {
  @override
  _ProviderDemoScreenState createState() => _ProviderDemoScreenState();
}

class _ProviderDemoScreenState extends State<ProviderDemoScreen> {
  @override
  void initState() {
    super.initState();
    final postMdl = Provider.of<PostDataProvider>(context, listen: false);
    postMdl.getPostData(context);
  }

  @override
  Widget build(BuildContext context) {
    final postMdl = Provider.of<PostDataProvider>(context);
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        padding: EdgeInsets.all(20),
        child: postMdl.loading
            ? Center(
                child: Container(
                  child: CircularProgressIndicator(),
                ),
              )
            : ListView.builder(
                itemBuilder: (ctx, index) => Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    postMdl.post.title ?? "",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                ),
              ),
      ),
    );
  }
}
