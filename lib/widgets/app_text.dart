import 'package:flutter/material.dart';

class AppText extends StatelessWidget {
  final String text;
  final Color styleTextColor;
  final double fontSize;
  final double latterSpace;
  final double wordSpace;
  final FontWeight fontWeight;
  final FontStyle fontStyle;

  const AppText({
    Key key,
    this.text,
    this.styleTextColor,
    this.fontSize = 12,
    this.fontStyle = FontStyle.normal,
    this.fontWeight = FontWeight.normal,
    this.latterSpace = 1.1,
    this.wordSpace = 1.1,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: styleTextColor,
        fontWeight: fontWeight,
        fontSize: fontSize,
        letterSpacing: latterSpace,
        fontStyle: fontStyle,
        wordSpacing: wordSpace,
      ),
    );
  }
}
