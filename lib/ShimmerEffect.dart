import 'package:flutter/material.dart';

import 'CardListItem.dart';

class ShimmerEffectPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: ShimmerEffect(),
    );
  }
}

class ShimmerEffect extends StatefulWidget {
  @override
  _ShimmerEffectState createState() => _ShimmerEffectState();
}

class _ShimmerEffectState extends State<ShimmerEffect> {
  List<String> list = ['+Add', 'view / edit', '+Add', 'view / edit'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white30,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 12),
              TextFormField(),
              SizedBox(
                height: 8,
              ),
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'Hint Text',
                ),
              ),
              SizedBox(
                height: 8,
              ),
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'Hint Text With Prefix Icon',
                  prefixIcon: Icon(Icons.help_outline),
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                  alignLabelWithHint: true,
                  counter: Text('1254'),
                  icon: Icon(Icons.add),
                  enabled: true,
                  labelText: 'Label Text',
                  isCollapsed: true,
                  counterText: '125478',

                  errorText: 'Error Text',
                  helperText: 'Helper Text',
                  suffixText: 'Suffix',
                  suffixStyle: TextStyle(color: Colors.black),
                  prefixText: 'Prefix',
                  prefixStyle: TextStyle(color: Colors.black),
                  suffixIcon: Icon(Icons.wb_sunny),
                  contentPadding:
                      EdgeInsets.only(top: 20), // add padding to adjust text
                  isDense: true,
                  hintText: "Email",
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10),
                    borderSide: new BorderSide(),
                  ),
                  prefixIcon: Icon(Icons.help_outline),
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                  alignLabelWithHint: true,
                  counter: Text('1254'),
                  icon: Icon(Icons.add),
                  enabled: true,
                  labelText: 'Label Text',
                  isCollapsed: true,
                  counterText: '125478',

                  errorText: 'Error Text',
                  helperText: 'Helper Text',
                  suffixText: 'Suffix',
                  suffixStyle: TextStyle(color: Colors.black),
                  prefixText: 'Prefix',
                  prefixStyle: TextStyle(color: Colors.black),
                  suffixIcon: Icon(Icons.wb_sunny),
                  contentPadding:
                      EdgeInsets.only(top: 20), // add padding to adjust text
                  isDense: true,
                  hintText: "Email",
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10),
                    borderSide: new BorderSide(),
                  ),
                  prefixIcon: Icon(Icons.help_outline),
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                  alignLabelWithHint: true,
                  counter: Text('1254'),
                  icon: Icon(Icons.add),
                  enabled: true,
                  labelText: 'Label Text',
                  isCollapsed: true,
                  counterText: '125478',

                  errorText: 'Error Text',
                  helperText: 'Helper Text',
                  suffixText: 'Suffix',
                  suffixStyle: TextStyle(color: Colors.black),
                  prefixText: 'Prefix',
                  prefixStyle: TextStyle(color: Colors.black),
                  suffixIcon: Icon(Icons.wb_sunny),
                  contentPadding:
                      EdgeInsets.only(top: 20), // add padding to adjust text
                  isDense: true,
                  hintText: "Email",
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10),
                    borderSide: new BorderSide(),
                  ),
                  prefixIcon: Icon(Icons.help_outline),
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                  alignLabelWithHint: true,
                  counter: Text('1254'),
                  icon: Icon(Icons.add),
                  enabled: true,
                  labelText: 'Label Text',
                  isCollapsed: true,
                  counterText: '125478',

                  errorText: 'Error Text',
                  helperText: 'Helper Text',
                  suffixText: 'Suffix',
                  suffixStyle: TextStyle(color: Colors.black),
                  prefixText: 'Prefix',
                  prefixStyle: TextStyle(color: Colors.black),
                  suffixIcon: Icon(Icons.wb_sunny),
                  contentPadding:
                      EdgeInsets.only(top: 20), // add padding to adjust text
                  isDense: true,
                  hintText: "Email",
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10),
                    borderSide: new BorderSide(),
                  ),
                  prefixIcon: Icon(Icons.help_outline),
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                  alignLabelWithHint: true,
                  counter: Text('1254'),
                  icon: Icon(Icons.add),
                  enabled: true,
                  labelText: 'Label Text',
                  isCollapsed: true,
                  counterText: '125478',

                  errorText: 'Error Text',
                  helperText: 'Helper Text',
                  suffixText: 'Suffix',
                  suffixStyle: TextStyle(color: Colors.black),
                  prefixText: 'Prefix',
                  prefixStyle: TextStyle(color: Colors.black),
                  suffixIcon: Icon(Icons.wb_sunny),
                  contentPadding:
                      EdgeInsets.only(top: 20), // add padding to adjust text
                  isDense: true,
                  hintText: "Email",
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10),
                    borderSide: new BorderSide(),
                  ),
                  prefixIcon: Icon(Icons.help_outline),
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                  alignLabelWithHint: true,
                  counter: Text('1254'),
                  icon: Icon(Icons.add),
                  enabled: true,
                  labelText: 'Label Text',
                  isCollapsed: true,
                  counterText: '125478',

                  errorText: 'Error Text',
                  helperText: 'Helper Text',
                  suffixText: 'Suffix',
                  suffixStyle: TextStyle(color: Colors.black),
                  prefixText: 'Prefix',
                  prefixStyle: TextStyle(color: Colors.black),
                  suffixIcon: Icon(Icons.wb_sunny),
                  contentPadding:
                      EdgeInsets.only(top: 20), // add padding to adjust text
                  isDense: true,
                  hintText: "Email",
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10),
                    borderSide: new BorderSide(),
                  ),
                  prefixIcon: Icon(Icons.help_outline),
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                  alignLabelWithHint: true,
                  counter: Text('1254'),
                  icon: Icon(Icons.add),
                  enabled: true,
                  labelText: 'Label Text',
                  isCollapsed: true,
                  counterText: '125478',

                  errorText: 'Error Text',
                  helperText: 'Helper Text',
                  suffixText: 'Suffix',
                  suffixStyle: TextStyle(color: Colors.black),
                  prefixText: 'Prefix',
                  prefixStyle: TextStyle(color: Colors.black),
                  suffixIcon: Icon(Icons.wb_sunny),
                  contentPadding:
                      EdgeInsets.only(top: 20), // add padding to adjust text
                  isDense: true,
                  hintText: "Email",
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10),
                    borderSide: new BorderSide(),
                  ),
                  prefixIcon: Icon(Icons.help_outline),
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                  alignLabelWithHint: true,
                  counter: Text('1254'),
                  icon: Icon(Icons.add),
                  enabled: true,
                  labelText: 'Label Text',
                  isCollapsed: true,
                  counterText: '125478',

                  errorText: 'Error Text',
                  helperText: 'Helper Text',
                  suffixText: 'Suffix',
                  suffixStyle: TextStyle(color: Colors.black),
                  prefixText: 'Prefix',
                  prefixStyle: TextStyle(color: Colors.black),
                  suffixIcon: Icon(Icons.wb_sunny),
                  contentPadding:
                      EdgeInsets.only(top: 20), // add padding to adjust text
                  isDense: true,
                  hintText: "Email",
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10),
                    borderSide: new BorderSide(),
                  ),
                  prefixIcon: Icon(Icons.help_outline),
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                  alignLabelWithHint: true,
                  counter: Text('1254'),
                  icon: Icon(Icons.add),
                  enabled: true,
                  labelText: 'Label Text',
                  isCollapsed: true,
                  counterText: '125478',

                  errorText: 'Error Text',
                  helperText: 'Helper Text',
                  suffixText: 'Suffix',
                  suffixStyle: TextStyle(color: Colors.black),
                  prefixText: 'Prefix',
                  prefixStyle: TextStyle(color: Colors.black),
                  suffixIcon: Icon(Icons.wb_sunny),
                  contentPadding:
                      EdgeInsets.only(top: 20), // add padding to adjust text
                  isDense: true,
                  hintText: "Email",
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10),
                    borderSide: new BorderSide(),
                  ),
                  prefixIcon: Icon(Icons.help_outline),
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                  alignLabelWithHint: true,
                  counter: Text('1254'),
                  icon: Icon(Icons.add),
                  enabled: true,
                  labelText: 'Label Text',
                  isCollapsed: true,
                  counterText: '125478',

                  errorText: 'Error Text',
                  helperText: 'Helper Text',
                  suffixText: 'Suffix',
                  suffixStyle: TextStyle(color: Colors.black),
                  prefixText: 'Prefix',
                  prefixStyle: TextStyle(color: Colors.black),
                  suffixIcon: Icon(Icons.wb_sunny),
                  contentPadding:
                      EdgeInsets.only(top: 20), // add padding to adjust text
                  isDense: true,
                  hintText: "Email",
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10),
                    borderSide: new BorderSide(),
                  ),
                  prefixIcon: Icon(Icons.help_outline),
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                  alignLabelWithHint: true,
                  counter: Text('1254'),
                  icon: Icon(Icons.add),
                  enabled: true,
                  labelText: 'Label Text',
                  isCollapsed: true,
                  counterText: '125478',

                  errorText: 'Error Text',
                  helperText: 'Helper Text',
                  suffixText: 'Suffix',
                  suffixStyle: TextStyle(color: Colors.black),
                  prefixText: 'Prefix',
                  prefixStyle: TextStyle(color: Colors.black),
                  suffixIcon: Icon(Icons.wb_sunny),
                  contentPadding:
                      EdgeInsets.only(top: 20), // add padding to adjust text
                  isDense: true,
                  hintText: "Email",
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10),
                    borderSide: new BorderSide(),
                  ),
                  prefixIcon: Icon(Icons.help_outline),
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                  alignLabelWithHint: true,
                  counter: Text('1254'),
                  icon: Icon(Icons.add),
                  enabled: true,
                  labelText: 'Label Text',
                  isCollapsed: true,
                  counterText: '125478',

                  errorText: 'Error Text',
                  helperText: 'Helper Text',
                  suffixText: 'Suffix',
                  suffixStyle: TextStyle(color: Colors.black),
                  prefixText: 'Prefix',
                  prefixStyle: TextStyle(color: Colors.black),
                  suffixIcon: Icon(Icons.wb_sunny),
                  contentPadding:
                      EdgeInsets.only(top: 20), // add padding to adjust text
                  isDense: true,
                  hintText: "Email",
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10),
                    borderSide: new BorderSide(),
                  ),
                  prefixIcon: Icon(Icons.help_outline),
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                  alignLabelWithHint: true,
                  counter: Text('1254'),
                  icon: Icon(Icons.add),
                  enabled: true,
                  labelText: 'Label Text',
                  isCollapsed: true,
                  counterText: '125478',

                  errorText: 'Error Text',
                  helperText: 'Helper Text',
                  suffixText: 'Suffix',
                  suffixStyle: TextStyle(color: Colors.black),
                  prefixText: 'Prefix',
                  prefixStyle: TextStyle(color: Colors.black),
                  suffixIcon: Icon(Icons.wb_sunny),
                  contentPadding:
                      EdgeInsets.only(top: 20), // add padding to adjust text
                  isDense: true,
                  hintText: "Email",
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10),
                    borderSide: new BorderSide(),
                  ),
                  prefixIcon: Icon(Icons.help_outline),
                ),
              ),
            ],
          ),
        ));
  }
}

class BuildTile extends StatelessWidget {
  final String status;

  BuildTile({Key key, @required this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      color: Colors.blue,
      child: Container(
        margin: EdgeInsets.only(left: 10),
        color: Colors.white,
        child: Row(
          children: [
            Container(
              color: Colors.yellow,
              margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Icon(
                Icons.add,
                size: 70,
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '15577',
                    style: TextStyle(color: Colors.blue, fontSize: 15),
                  ),
                  Text(
                    '000000000000000000000',
                    style: TextStyle(color: Colors.blue, fontSize: 15),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(5, 0, 20, 0),
              child: Text(status),
            )
          ],
        ),
      ),
    );
  }
}
