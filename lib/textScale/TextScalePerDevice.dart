import 'package:flutter/material.dart';

class TextScaleFactor extends StatelessWidget {
  MediaQueryData queryData;

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);
    double devicePixelRatio = queryData.devicePixelRatio;

    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Scale Size Text'),
      ),
      body: new Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          new Text(
            'Saiyed Mustaq $devicePixelRatio',
            style: style12,
          ),
          new Text(
            'Saiyed Mustaq $devicePixelRatio',
            style: style14,
          ),
          new Text(
            'Saiyed Mustaq $devicePixelRatio',
            style: style16,
          ),
          new Text(
            'Saiyed Mustaq $devicePixelRatio',
            style: style18,
          ),
          new Text(
            'Saiyed Mustaq $devicePixelRatio',
            style: style20,
          ),
          new Text(
            'Saiyed Mustaq $devicePixelRatio',
            style: style22,
          ),
          new Text(
            'Saiyed Mustaq $devicePixelRatio',
            style: style24,
          ),
          new Text(
            'Saiyed Mustaq ${devicePixelRatio}',
            style: style26,
          ),
        ],
      ),
    );
  }
}

TextStyle style12 = new TextStyle(
  inherit: true,
  fontSize: 12.0,
);
TextStyle style14 = new TextStyle(
  inherit: true,
  fontSize: 14.0,
);
TextStyle style16 = new TextStyle(
  inherit: true,
  fontSize: 16.0,
);
TextStyle style18 = new TextStyle(
  inherit: true,
  fontSize: 18.0,
);
TextStyle style20 = new TextStyle(
  inherit: true,
  fontSize: 20.0,
);
TextStyle style22 = new TextStyle(
  inherit: true,
  fontSize: 22.0,
);
TextStyle style24 = new TextStyle(
  inherit: true,
  fontSize: 24.0,
);
TextStyle style26 = new TextStyle(
  inherit: true,
  fontSize: 26.0,
);
