import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'PostModel.dart';

class PostDataProvider with ChangeNotifier {
  PostModel post = PostModel();
  bool loading = false;

  getPostData(context) async {
    loading = true;
    post = await getSinglePostData(context);
    loading = false;
    notifyListeners();
  }

  Future<PostModel> getSinglePostData(context) async {
    PostModel postModelResult;
    final String url = "https://jsonplaceholder.typicode.com/posts/1";
    try {
      final http.Response response = await http.get(Uri.parse(url),
          headers: {HttpHeaders.contentTypeHeader: "application/json"});
      if (response.statusCode == 200) {
        final item = jsonDecode(response.body);
        postModelResult = PostModel.fromJson(item);
      } else {
        log("Data Not Found");
      }
    } catch (e) {
      log(e);
    }
    return postModelResult;
  }
}
